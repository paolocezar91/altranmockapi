var passport = require('passport');
var config = require('./auth');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var LocalStrategy = require('passport-local').Strategy;
const http = require('http')

var localOptions = {
	usernameField: 'email',
	passwordField: 'password',
	passReqToCallback : true
};

var localLogin = new LocalStrategy(localOptions, (req, email, password, done) => {
	
	// This methods makes a get request to the Altran Mockup API and parsing it to the request done by the route
	// Usually this would be a request to a database, like mongodb, using mongoose
	http.get({
		host: 'www.mocky.io',
		path: '/v2/5808862710000087232b75ac',
	}, (response) => {
			var body = '';
			response.on('data', (d) => {
				body += d;
			});
			response.on('end', () => {
				var parsed = JSON.parse(body),
				
				// this is a comparison using email, but usually one would also compare the password against a hash, storaged on database
				client = parsed.clients.find((client) => {
					return client.email == req.body.email					
				})

				return done(null, client);
			});
	});

});
 
var jwtOptions = {
	jwtFromRequest: ExtractJwt.fromAuthHeader(),
	secretOrKey: config.secret
};

var jwtLogin = new JwtStrategy(jwtOptions, (payload, done) => {

	// This methods makes a get request to the Altran Mockup API and parsing it to the request done by the route
	// Usually this would be a request to a database, like mongodb, using mongoose
 	http.get({
		host: 'www.mocky.io',
		path: '/v2/5808862710000087232b75ac',
	}, (response) => {
			var body = '';
			response.on('data', (d) => {
				body += d;
			});
			response.on('end', () => {
				var parsed = JSON.parse(body),
				
				client = parsed.clients.find((client) => {
					return client.id == payload.id
				})

				return done(null, client);
			});
	});

});

passport.use(jwtLogin);
passport.use(localLogin);