var passportService = require('../config/passport'),
	passport = require('passport'),
	requireAuth = passport.authenticate('jwt', {session: false}),
  requireLogin = passport.authenticate('local', {session: false, badRequestMessage: "Digite as credenciais"});

var authService = require('../services/auth.js'),

	filters = {
		login: requireLogin,
		auth: requireAuth,
    user: authService.roleAuthorization( 'user' ),
    admin: authService.roleAuthorization( 'admin' ),
    //auth: passport.authenticate( 'jwt', {session: false} ),
    //login: passport.authenticate( 'local', {session: false} )
	}

module.exports = filters