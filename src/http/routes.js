module.exports = [
	{
		path: "/auth",
		subroutes: [
			{
				method: "post",
				filters: ["login"],
				path: "/login",
				ctrl: "auth",
				callback: "login"
			}
		]
	},
	{
		path: "/users",
		subroutes: [
			{
				method: "get", 
				filters: ['auth'], 
				path: "/",
				ctrl: "users",
				callback: "getAll"
			},
			{
				method: "get", 
				filters: ['auth'], 
				path: "/id/:id", 
				ctrl: "users", 
				callback: "getOneById"
			},
			{
				method: "get", 
				filters: ['auth'], 
				path: "/name/:name", 
				ctrl: "users", 
				callback: "getOneByName"
			}
		]
	}, 
	{
		path: "/policies",
		subroutes: [
			{
				method: "get",
				filters: ['auth', 'admin'],
				path: "/",
				ctrl: "policies",
				callback: "getAll"
			},
			{
				method: "get", 
				filters: ['auth', 'admin'], 
				path: "/username/:username", 
				ctrl: "policies", 
				callback: "getPoliciesByUsername"
			},
			{
				method: "get", 
				filters: ['auth', 'admin'], 
				path: "/userByLicenseId/:license_id", 
				ctrl: "policies", 
				callback: "getUserByLicenseId"
			}
		]
	}
]