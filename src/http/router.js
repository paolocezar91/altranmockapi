// Dependencies
const express = require('express');

module.exports = (routes, filters) => {
	var mainRouter = express.Router(), subRouter;

	//Building routes: uses routes.js as object to create routes
	routes.forEach((route) => {		
		subRouter = express.Router();
		
		route.subroutes.forEach((subroute) => {
			if(subroute.ctrl){
				// building middleware: pushing each function on filter.js as required by each route
				// example: middleware = [authCtrl.roleAuthorization( 'user' ), authCtrl.roleAuthorization( 'admin' )]
				var middleware = [];				

				subroute.filters.forEach((f) => {
					middleware.push(filters[f]);
				})

				// getting controller required by route
				// example: ctrl = require("./api/users.js")
				var ctrl = require("../api/" + subroute.ctrl + ".js")
				// subRouter["get"]("/", ctrl["getAll"])

				subRouter[subroute.method](subroute.path, middleware, ctrl[subroute.callback])
			}
		})
		mainRouter.use(route.path, subRouter);
		
	})

	return mainRouter
}