var express  = require('express'),
	app = express(),                               // create our app w/ express
	http = require('http'),
	morgan = require('morgan'),             // log requests to the console (express4)
	bodyParser = require('body-parser'),    // pull information from HTML POST (express4)
	methodOverride = require('method-override'), // simulate DELETE and PUT (express4)
	cors = require('cors'),
	path = require('path'),
	
	session = require('express-session');

console.log("** STARTING AltranMockupAPI **")
console.log('ENVIROMENT=dev\n')

app.use(morgan( 'dev' ));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());
app.use(cors());

app.use(session({
	secret: 'AltranMockupAPI',
	resave: false,
	saveUninitialized: true
}));

app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*")
	res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE')
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")

	next()
});

// Get router function, routes object and filters object
var	routes = require('./http/routes'),
	filters = require('./http/filters'),
	router = require('./http/router');

// binds router to main app 
app.use('/api', router(routes, filters) )

// listen (start app with node server.js)
http.createServer(app).listen(8080)
console.log("http server listening on port 8080")	