const http = require('http')

exports.getAll = (req, res) => {

	// This methods makes a get request to the Altran Mockup API and parsing it to the request done by the route
	// Usually this would be a request to a database, like mongodb, using mongoose
	http.get({
		host: 'www.mocky.io',
		path: '/v2/5808862710000087232b75ac',
	}, (response) => {
			var body = '';
			response.on('data', (d) => {
				body += d;
			});
			response.on('end', () => {
				var parsed = JSON.parse(body);
				res.status(200).send(parsed.clients);
			});
	});

}

exports.getOneById = (req, res) => {
	http.get({
		host: 'www.mocky.io',
		path: '/v2/5808862710000087232b75ac',
	}, (response) => {
			var body = '';
			response.on('data', (d) => {
				body += d;
			});
			response.on('end', () => {
				var parsed = JSON.parse(body),
				
				user = parsed.clients.find((client) => {
					return client.id == req.params.id					
				})

				res.status(200).send(user);
			});
	});
}

exports.getOneByName = (req, res) => {
	http.get({
		host: 'www.mocky.io',
		path: '/v2/5808862710000087232b75ac',
	}, (response) => {
			var body = '';
			response.on('data', (d) => {
				body += d;
			});
			response.on('end', () => {
				var parsed = JSON.parse(body),
				
				user = parsed.clients.find((client) => {
					return client.name == req.params.name					
				})

				res.status(200).send(user);
			});
	});
}