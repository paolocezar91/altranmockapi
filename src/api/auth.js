var jwt = require('jsonwebtoken');
var authConfig = require('../config/auth'),
	authService = require('../services/auth')

exports.login = (req, res) => {
	var userInfo = authService.setUserInfo(req.user);   

	res.status(200).json({
		token: 'JWT ' + authService.generateToken(userInfo),
		user: userInfo
	});
}