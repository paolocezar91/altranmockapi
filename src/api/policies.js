const http = require('http'),
	async = require('async')

exports.getAll = (req, res) => {    
	http.get({
		host: 'www.mocky.io',
		path: '/v2/580891a4100000e8242b75c5',
	}, (response) => {
			var body = '';
			response.on('data', (d) => {
				body += d;
			});
			response.on('end', () => {
				console.log(body)
				var parsed = JSON.parse(body);
				res.status(200).send(parsed.policies);
			});
	});
}

exports.getPoliciesByUsername = (req, res) => {
	// Using async to waterfall the asynchronous request and execute them in order, as the result of the second is dependent on the first
	async.waterfall([
		(cb) => {

			http.get({
				host: 'www.mocky.io',
				path: '/v2/5808862710000087232b75ac',
			}, (response) => {
				var body = '';
				response.on('data', (d) => {
					body += d;
				});
				response.on('end', () => {
					var parsed = JSON.parse(body),

					client = parsed.clients.find((client) => {
						return client.name == req.params.username
					})

					cb(null, client)
				});
			});

		}, 
		(client, cb) => {
			
			http.get({
				host: 'www.mocky.io',
				path: '/v2/580891a4100000e8242b75c5',
			}, (response) => {
				var body = '';
				response.on('data', (d) => {
					body += d;
				});
				response.on('end', () => {
					var parsed = JSON.parse(body),
					// using filter instead of find
					policies = parsed.policies.filter((p) => {
						return p.clientId == client.id
					})

					cb(policies)
				});

			});
		}
	], (policies) => res.status(200).send(policies))
}

exports.getUserByLicenseId = (req, res) => {
	async.waterfall([
		(cb) => {
			http.get({
				host: 'www.mocky.io',
				path: '/v2/580891a4100000e8242b75c5',
			}, (response) => {
					var body = '';
					response.on('data', (d) => {
						body += d;
					});
					response.on('end', () => {
						var parsed = JSON.parse(body),
						
						policy = parsed.policies.find((p) => {
							return p.id == req.params.license_id					
						})

						cb(null, policy)
					});
			});

		},
		(policy, cb) => {

			http.get({
				host: 'www.mocky.io',
				path: '/v2/5808862710000087232b75ac',
			}, (response) => {
				var body = '';
				response.on('data', (d) => {
					body += d;
				});
				response.on('end', () => {
					var parsed = JSON.parse(body),
					user = parsed.clients.find((client) => {
						return client.id == policy.clientId
					})

					cb(user)
				});				
			});

		}
	],
	(user) => res.status(200).send(user)
	)
}