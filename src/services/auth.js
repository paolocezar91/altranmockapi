var jwt = require('jsonwebtoken');
var authConfig = require('../config/auth');

exports.generateToken = (user) => {
	return jwt.sign(user, authConfig.secret, {
		expiresIn: '30 days'
	});
}

exports.setUserInfo = (user) => {
	console.log({user})
	return {
		id: user.id,
		email: user.email,
		name: user.name,
		role: user.role,
	};
}

exports.roleAuthorization = (role) => {
	return (req, res, next) => {
		if(req.user.role == role)
			return next()
		else
			return next("Error, should have " + role + " role")
	}
}