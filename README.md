# README #

This is the README for Altran Mock API test.

### How do I get set up? ###

First, pull the git repository.

You should have at leasat node v6.11.1 and npm 3.10.10 to run. 

Run `npm install` to install all dependencies

Run `npm start` to run the project with node as daemon on development mode, with livereload

Run `npm serve`to run the project as distribution

Suggestion: use Postman to import `Altran.postman_collection.json` to test all relevant routes. User and Admin login have been setup as well. They generate a JWT token that must be passed as authorization on header for other requests.